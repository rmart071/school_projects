Converter Application

How to use application:

Home Screen (Main Menu)
1. Once you open the application, you will be presented with the main menu.
2. The main menu will present different buttons, each representing a different conversion type
3. Select a conversion type


Conversion Screens
Whichever option was selected, the application will direct you to their pertaining view. You will have
a similar layout in all views. The views will have a text field where you can insert any number and 
two checkboxes with a conversion type. 
1. You can select the conversion type and enter your number
2. You can select the other conversion type to see the difference between the two measurement types

In order to go to the other conversion screens, you can click the back button on your android device,
which will direct you back to the home screen and select other option 
