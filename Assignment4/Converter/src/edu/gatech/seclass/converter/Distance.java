package edu.gatech.seclass.converter;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;

public class Distance extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_distance);
	}
	
	public void handleClick(View view){
		double distance = 0.0;
        boolean checked = ((RadioButton) view).isChecked();

        EditText txt = (EditText) findViewById(R.id.text1);
        

        switch (view.getId()){
            case R.id.radioButton:
                if(checked && txt.length() > 0){
                    distance = Double.parseDouble(txt.getText().toString());
                    txt.setText(kmToMiles(distance));
                }
                break;
            case R.id.radioButton2:
                if(checked && txt.length() > 0){
                		distance = Double.parseDouble(txt.getText().toString());
                    txt.setText(milesToKm(distance));   
                }
                break;
        }
    }

    public String milesToKm(double miles){
        double km = miles * 1.609;
        return String.valueOf(km);
    }


    public String kmToMiles(double km){
        double miles = km / 1.609;
        return String.valueOf(miles);
    }
}
