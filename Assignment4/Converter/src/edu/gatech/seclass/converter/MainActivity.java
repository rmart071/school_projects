package edu.gatech.seclass.converter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;

public class MainActivity extends Activity {

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
    
  
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void changeActivity(View view){
    	switch (view.getId()){
        case R.id.button_area:
        		Intent intent = new Intent(MainActivity.this, Area.class);
        		startActivity(intent);
        		break;
        case R.id.button_currency:
        		Intent intent1 = new Intent(MainActivity.this, Currency.class);
        		startActivity(intent1);
        		break;
        case R.id.button_distance:
    			Intent intent2 = new Intent(MainActivity.this, Distance.class);
    			startActivity(intent2);
    			break;
        case R.id.button_fluid:
    			Intent intent3 = new Intent(MainActivity.this, Fluid.class);
    			startActivity(intent3);
    			break;
        case R.id.button_weight:
    			Intent intent4 = new Intent(MainActivity.this, Weight.class);
    			startActivity(intent4);
    			break;
    }
        
    }
}
