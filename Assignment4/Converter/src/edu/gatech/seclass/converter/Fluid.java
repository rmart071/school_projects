package edu.gatech.seclass.converter;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;

public class Fluid extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fluid);
	}
	
	public void handleClick(View view){
		double fluid = 0.0;
        boolean checked = ((RadioButton) view).isChecked();

        EditText txt = (EditText) findViewById(R.id.text1);
        

        switch (view.getId()){
            case R.id.radioButton:
                if(checked && txt.length() > 0){
                		fluid = Double.parseDouble(txt.getText().toString());
                    txt.setText(litersToLqOz(fluid));
                }
                break;
            case R.id.radioButton2:
                if(checked && txt.length() > 0){
            			fluid = Double.parseDouble(txt.getText().toString());
                    txt.setText(lqOzToLiters(fluid));
                }
                break;
        }
    }

    public String lqOzToLiters(double lqOz){
        double liters = lqOz / 33.814;
        return String.valueOf(liters);
    }
    
    public String litersToLqOz(double liters){
        double lqOz = liters * 33.814;
        return String.valueOf(lqOz);
    }
}
