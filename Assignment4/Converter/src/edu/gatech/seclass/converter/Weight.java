package edu.gatech.seclass.converter;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;

public class Weight extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_weight);
	}
	
	
	public void handleClick(View view){
		double weight = 0.0;
        boolean checked = ((RadioButton) view).isChecked();

        EditText txt = (EditText) findViewById(R.id.text1);
        

        switch (view.getId()){
            case R.id.radioButton:
                if(checked && txt.length() > 0){
                		weight = Double.parseDouble(txt.getText().toString());
                    txt.setText(kgToLbs(weight));
                }
                break;
            case R.id.radioButton2:
                if(checked && txt.length() > 0){
            			weight = Double.parseDouble(txt.getText().toString());
                    txt.setText(lbsToKg(weight));
                }
                break;
        }
    }

    public String lbsToKg(double lbs){
        double kg = lbs / 2.2046;
        return String.valueOf(kg);
    }
    
    public String kgToLbs(double kg){
        double lbs = kg * 2.2046;
        return String.valueOf(lbs);
    }
}
