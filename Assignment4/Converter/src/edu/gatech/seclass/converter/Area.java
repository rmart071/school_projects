package edu.gatech.seclass.converter;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;

public class Area extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_area);
	}
	
	public void handleClick(View view){
		double area = 0.0;
        boolean checked = ((RadioButton) view).isChecked();

        EditText txt = (EditText) findViewById(R.id.text1);
        

        switch (view.getId()){
            case R.id.radioButton:
                if(checked && txt.length() > 0 ){
                		area = Double.parseDouble(txt.getText().toString());
                    txt.setText(sqMToSqFt(area));
                }
                break;
            case R.id.radioButton2:
                if(checked && txt.length() > 0){
                		area = Double.parseDouble(txt.getText().toString());
                    txt.setText(sqFtToSqM(area));
                }
        }
    }

    public String sqFtToSqM(double feet){
        double meters = feet/10.764;
        return String.valueOf(meters);
    }
    
    public String sqMToSqFt(double meters){
        double feet = meters * 10.764;
        return String.valueOf(feet);
    }
}
