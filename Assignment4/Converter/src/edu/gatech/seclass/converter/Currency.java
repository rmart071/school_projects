package edu.gatech.seclass.converter;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;

public class Currency extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_currency);
	}
	
	public void handleClick(View view){
		double currency = 0.0;
        boolean checked = ((RadioButton) view).isChecked();

        EditText txt = (EditText) findViewById(R.id.text1);
        

        switch (view.getId()){
            case R.id.radioButton:
                if(checked && txt.length() > 0){
                		currency = Double.parseDouble(txt.getText().toString());
                    txt.setText(eurosToUS(currency));
                }
                break;
            case R.id.radioButton2:
                if(checked && txt.length() > 0){
                		currency = Double.parseDouble(txt.getText().toString());
                    txt.setText(usToEuros(currency));
                }
                break;
        }
    }

    public String usToEuros(double dollars){
        double euros = dollars * 0.89536;
        return String.valueOf(euros);
    }
    
    public String eurosToUS(double euros){
        double dollars = euros / 0.89536;
        return String.valueOf(dollars);
    }
}
