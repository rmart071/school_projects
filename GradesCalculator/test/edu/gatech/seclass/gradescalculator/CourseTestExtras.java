package edu.gatech.seclass.gradescalculator;

import static org.junit.Assert.*;

import java.util.HashSet;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CourseTestExtras {
	
	Students students = null;
    Grades grades = null;
    Course course = null;
    static final String STUDENTS_DB = "DB/GradesDatabase6300-students.xlsx";
    static final String GRADES_DB = "DB/GradesDatabase6300-grades.xlsx";

    @Before
    public void setUp() throws Exception {
        students = new Students(STUDENTS_DB);
        grades = new Grades(GRADES_DB);
        course = new Course(students, grades);
    }

    @After
    public void tearDown() throws Exception {
        students = null;
        grades = null;
        course = null;
    }

	
    // test case will throw exception since user name does not exist
	@Test
    public void testGetStudentsByName1() throws Exception{
        Student student = course.getStudentByName("Yellow Jacket");
        try {
        		assertEquals("", student.getGtid());
        } catch (NullPointerException expected) {
            
        }
    }
	
	// test case will throw exception since user id does not exist
	@Test
    public void testGetStudentById1() throws Exception{
        Student student = course.getStudentByID("32");
        try {
        		assertEquals("", student.getName());
        } catch (NullPointerException expected) {
            
        }
    }
	
	@Test
	public void testGetStudentById2(){
		HashSet<Student> studentsRoster = null;
        studentsRoster = course.getStudents();
        Student student = null;
        for (Student s : studentsRoster) {
            if (s.getGtid().equals("901234501")) {
                student = s;
                break;
            }
        }
        assertEquals("Freddie Catlay",student.getName());
	}
	
	@Test
	public void testGetStudentByName2(){
		HashSet<Student> studentsRoster = null;
        studentsRoster = course.getStudents();
        Student student = null;
        for (Student s : studentsRoster) {
            if (s.getName().equals("Wilfrid Eastwood")) {
                student = s;
                break;
            }
        }
        assertEquals("901234511",student.getGtid());
	}
	
	@Test
	public void testGetTeamById1(){
		Student student = course.getStudentByID("901234507");
		assertEquals("Team 1", student.getTeam());
	}
	
	@Test
	public void testGetAttendanceById1(){
		Student student = course.getStudentByID("901234507");
		assertEquals(100, student.getAttendance());
	}

}
