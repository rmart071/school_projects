package edu.gatech.seclass.gradescalculator;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.sun.xml.internal.bind.v2.schemagen.xmlschema.List;

public class Students {
	XSSFSheet sheet1;
	XSSFSheet sheet2;
	XSSFWorkbook workbook;
	Set<Student> students = new HashSet<Student>();
	Student student;

	public Students(String database) {
		String name = null;
		int id = 0;
		String email = null;
		try {
			FileInputStream file = new FileInputStream(new File(database));

			// Get the workbook instance for XLS file
			workbook = new XSSFWorkbook(file);
			// Students Info
			sheet1 = workbook.getSheetAt(0);
			// Teams
			sheet2 = workbook.getSheetAt(1);
			
			int count = rowCount(sheet1);
			int line = 1;
			
			
			while (line <= count) {
				Row row = sheet1.getRow(line);
				for (Cell cell : row) {
					switch (cell.getCellType()) {
					case Cell.CELL_TYPE_STRING:
						if(cell.getStringCellValue().contains("@"))
							email = cell.getStringCellValue();
						else if(cell.getStringCellValue().length() > 1)
							name = cell.getStringCellValue();
						break;
					case Cell.CELL_TYPE_NUMERIC:
						if(String.valueOf(cell.getNumericCellValue()).length() >= 9)
							id = (int) cell.getNumericCellValue();
						break;
					}

				}
				student = new Student(name, String.valueOf(id));
				student.setEmail(email);
				students.add(student);
				findTeam();
								
				line++;
			}

		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public int rowCount(XSSFSheet sheet) {
		sheet = workbook.getSheetAt(0);
		return sheet.getLastRowNum();
	}

	public int studentCount() {
		return students.size();
	}

	public void findTeam() {
		String team_name = "";
		int id = 0;
		for (Row row : sheet2) {
			for (Cell cell : row) {
				switch (cell.getCellType()) {
				case Cell.CELL_TYPE_STRING:
					if (cell.getStringCellValue().contains("Team") && !cell.getStringCellValue().contains("#"))
						team_name = cell.getStringCellValue();
					break;
				case Cell.CELL_TYPE_NUMERIC:
					id = (int) cell.getNumericCellValue();
					for (Student obj : students) {
						if (obj.getGtid().equals(String.valueOf(id))) {
							obj.setTeam(team_name);
							break;
						}
					}
					break;
				}

			}
		}

	}

}
