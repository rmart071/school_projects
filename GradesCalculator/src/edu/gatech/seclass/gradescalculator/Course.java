package edu.gatech.seclass.gradescalculator;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

public class Course {
	public int numStudents;
	public int numAssignments;
	public int numProjects;
	Set<Student> students = new HashSet<Student>();
	public Grades grade;

	public Course(Students students2, Grades grades2) {
		students = students2.students;
		grade = grades2;
		setNumStudents(students2.studentCount());
		setNumAssignments(grades2.assignmentCount());
		setNumProjects(grades2.projectCount());
	}

	public int getNumStudents() {
		return numStudents;
	}

	public void setNumStudents(int numStudents) {
		this.numStudents = numStudents;
	}

	public int getNumAssignments() {
		return grade.assignmentCount();
	}

	public void setNumAssignments(int numAssignments) {
		this.numAssignments = numAssignments;
	}

	public int getNumProjects() {
		return numProjects;
	}

	public void setNumProjects(int numProjects) {
		this.numProjects = numProjects;
	}

	public HashSet<Student> getStudents() {
		return (HashSet<Student>) students;
	}

	public Grades getGrade() {
		return grade;
	}

	public void setGrade(Grades grade) {
		this.grade = grade;
	}

	public Student getStudentByID(String id) {
		int attendance;
		for (Student obj : students) {
			if (obj.getGtid().equals(id)) {
				attendance = grade.getAttendance(students, id);
				obj.setAttendance(attendance);
				return obj;
			}
		}
		return null;
	}

	public Student getStudentByName(String name) {
		int attendance;
		for (Student obj : students) {
			if (obj.getName().equals(name)) {
				attendance = grade.getAttendance(students, obj.getGtid());
				obj.setAttendance(attendance);
				return obj;
			}
		}
		return null;
	}

	public void addAssignment(String string) {
		grade.addAssignment(string);
		
	}

	public void updateGrades(Grades grades) {
		grade = grades;
		
	}

	public void addGradesForAssignment(String assignmentName, HashMap<Student, Integer> grades) {
		grade.addGradesForAssignment(assignmentName,grades);
		
	}

	public int getAverageAssignmentsGrade(Student student1) {
		return grade.getAverageAssignmentsGrade(student1.getGtid());
	}

	public void addIndividualContributions(String projectName1, HashMap<Student, Integer> contributions1) {
		grade.addIndividualContributions(projectName1, contributions1);
		
	}

	public int getOverallGrade(Student student) throws GradeFormulaException {
		Student student2;
		student2 = this.getStudentByID(student.getGtid());
		int AT, AA, AP;
		
		AT = this.getAttendance(student2);
		AA = this.getAverageAssignmentsGrade(student2);
		AP = this.getAverageProjectsGrade(student2);
		
		return grade.calculateOverallGrade(AT, AA, AP);
	}

	public String getTeam(Student student) {
		return student.getTeam();
	}

	public int getAttendance(Student student) {
		return student.getAttendance();
	}

	public String getEmail(Student student) {
		return student.getEmail();
	}

	public String getFormula() {
		return grade.getFormula();
	}

	public void setFormula(String text) {
		grade.setFormula(text);
	}

	public int getAverageProjectsGrade(Student student1) {
		Student student = getStudentByID(student1.getGtid());
		return grade.getAverageProjectsGrade(student.getGtid(), student.getTeam());
	}


}
