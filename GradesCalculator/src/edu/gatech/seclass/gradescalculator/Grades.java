package edu.gatech.seclass.gradescalculator;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.sun.org.apache.xalan.internal.xsltc.compiler.Pattern;

public class Grades {
	XSSFWorkbook workbook;
	XSSFSheet sheet;
	XSSFSheet sheet2;
	String database1;
	String formula = "AT * 0.2 + AA * 0.4 + AP * 0.4";
	String operators = "[ \\*\\/\\+\\-]+";
	

	public Grades(String database) {
		try {
			FileInputStream file = new FileInputStream(new File(database));
			database1 = database;
			// Get the workbook instance for XLS file
			workbook = new XSSFWorkbook(file);

		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	//counts the number of columns in excel sheet
	//removes the counting of first column since it is showing GT id's
	public int assignmentCount() {
		sheet = workbook.getSheetAt(1);
		Iterator<Row> rowIterator = sheet.iterator();
		int assigmentCount = 0;
		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			assigmentCount = row.getLastCellNum();
			break;
		}
		return assigmentCount - 1;

	}
	
	
	/*
	 * @param: Set of Student, GT id
	 * @return attendance value
	 * This function iterates through the set and retrieves attendance
	 * value when id matches the id in the set
	 */
	public int getAttendance(Set<Student> students2, String gtid) {
		int studentAttendance = 0;
		sheet = workbook.getSheetAt(0);
		int count = sheet.getLastRowNum();
		int attendance = 0, gtid1 = 0;
		int line = 1;
		while (line <= count) {
			
			Row row = sheet.getRow(line);
			for (Cell cell : row) {
				switch (cell.getCellType()) {
				case Cell.CELL_TYPE_STRING:
					break;
				case Cell.CELL_TYPE_NUMERIC:
					if(String.valueOf(cell.getNumericCellValue()).length() >= 9)
						gtid1 = (int) cell.getNumericCellValue();
					else
						attendance = (int) cell.getNumericCellValue();
					break;
				}
			}
			for (Student obj : students2) {
				if (gtid.equals(String.valueOf(gtid1))) {
					studentAttendance = attendance;
					break;
				}
			}
			line++;

		}
		return studentAttendance;
	}

	
	public int projectCount() {
		int projectCount = 0;
		sheet = workbook.getSheetAt(2);
		Row row2 = sheet.getRow(0);
		projectCount = row2.getLastCellNum();
		
		return projectCount - 1;
	}
	
	public int getAverageAssignmentsGrade(String gtid){
		sheet = workbook.getSheetAt(1);
		Iterator<Row> rowIterator = sheet.iterator();
		double assignmentSum = 0.0;
		int line = 1;
		int count = this.assignmentCount();
		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			
			for (Cell cell : row) {
				switch (cell.getCellType()) {
				case Cell.CELL_TYPE_STRING:
					break;
				case Cell.CELL_TYPE_NUMERIC:
					if(!gtIdMatchesValue(gtid,String.valueOf((int)cell.getNumericCellValue())))
						break;
					else{
						while(line <= count){
							assignmentSum += row.getCell(line).getNumericCellValue();
							line++;
						}
						return (int) Math.round(assignmentSum/count);
						
					}
				}

			}
		}
		return 0;
	}

	public void setFormula(String string) {
		formula = string;
	}

	public int getAverageProjectsGrade(String gtid, String team) {
		//IndividualContribs
		sheet = workbook.getSheetAt(2);
		Iterator<Row> rowIterator = sheet.iterator();
		//TeamGrades
		sheet2 = workbook.getSheetAt(3);
		Iterator<Row> rowIterator2 = sheet2.iterator();
		int project_count = this.projectCount();
		int line = 1;
		int iterator = 0;
		int average = 0;
		float sum = 0;
		
		ArrayList<Float> contrib = new ArrayList<Float>();
		ArrayList<Float> grade = new ArrayList<Float>();
		
		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			
			for (Cell cell : row) {
				switch (cell.getCellType()) {
				case Cell.CELL_TYPE_STRING:
					
					break;
				case Cell.CELL_TYPE_NUMERIC:
					if(!gtIdMatchesValue(gtid,String.valueOf((int)cell.getNumericCellValue())))
						break;
					else{
						while(line <= project_count){
							contrib.add((float) row.getCell(line).getNumericCellValue());
							line++;
						}
						
					}
				}

			}
		}
		
		line = 1;
		while (rowIterator2.hasNext()) {
			Row row2 = rowIterator2.next();
			
			for (Cell cell : row2) {
				switch (cell.getCellType()) {
				case Cell.CELL_TYPE_STRING:
					if(team.equals(cell.getStringCellValue())){
						while(line <= project_count){
							grade.add((float) row2.getCell(line).getNumericCellValue());
							line++;
						}
						continue;
					}else
						break;
				case Cell.CELL_TYPE_NUMERIC:
					break;
				}

			}
		}
		
		while(iterator < project_count){
			sum += (contrib.get(iterator)/100)*grade.get(iterator);
			iterator++;
		}
		
		average = Math.round(sum/iterator);
		return average;
	}

	public void addAssignment(String string) {
		int count = this.assignmentCount();
		//assignmentTabs
		sheet = workbook.getSheetAt(1);
		Row row = sheet.getRow(0);
		
		Cell cell = row.getCell(count+1);
		
		if (cell == null) {
	        cell = row.createCell(count+1);
	    }
	    cell.setCellType(Cell.CELL_TYPE_STRING);
	    cell.setCellValue(string);
	    
	    
	    FileOutputStream fileOut = null;
	    try {
			fileOut = new FileOutputStream(database1);
		} catch (FileNotFoundException e) {
			System.out.println("File was not found");
			e.printStackTrace();
		}
	    try {
			workbook.write(fileOut);
		} catch (IOException e) {
			System.out.println("File could not be written to");
			e.printStackTrace();
		}
	    
	    try {
			fileOut.close();
		} catch (IOException e) {
			System.out.println("File could not be closed");
			e.printStackTrace();
		}
	    		
	}
	
	public void addGradesForAssignment(String assignmentName, HashMap<Student, Integer> grades) {
		int count = this.assignmentCount();
		int position = 0, line = 0, rowValue = 0;
		//assignmentTabs
		sheet = workbook.getSheetAt(1);
		
		//get position where assignment name is present
		//and store in position variable
		Row headerRow = sheet.getRow(0);
		while(line <= count){
			if(headerRow.getCell(line) != null &&(headerRow.getCell(line).getStringCellValue().equals(assignmentName))){
				position = line;
			}
				line++;
		}
		
		Iterator it = grades.entrySet().iterator();
		while (it.hasNext()) {
			Iterator<Row> rowIterator = sheet.iterator();
	        Map.Entry pair = (Map.Entry)it.next();

	        while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				for (Cell cell : row) {
					
					switch (cell.getCellType()) {
					case Cell.CELL_TYPE_STRING:
							break;
					case Cell.CELL_TYPE_NUMERIC:

						if(((Student) pair.getKey()).getGtid().equals(String.valueOf((int)cell.getNumericCellValue()))){
							rowValue = cell.getRowIndex();
						}
						break;
					}

				}
				if(rowValue > 0){
					Row newRow = sheet.getRow(rowValue);
					Cell newCell = newRow.createCell(position);
					newCell.setCellType(Cell.CELL_TYPE_NUMERIC);
					newCell.setCellValue((int) pair.getValue());
					rowValue = 0;
				}
			}
	    }
		
		
		FileOutputStream fileOut = null;
		try {
			fileOut = new FileOutputStream(database1);
		} catch (FileNotFoundException e) {
			System.out.println("File was not found");
			e.printStackTrace();
		}
	    try {
			workbook.write(fileOut);
		} catch (IOException e) {
			System.out.println("File could not be written to");
			e.printStackTrace();
		}
	    
	    try {
			fileOut.close();
		} catch (IOException e) {
			System.out.println("File could not be closed");
			e.printStackTrace();
		}
			    	
	}

	public void addIndividualContributions(String projectName1, HashMap<Student, Integer> contributions1) {
		sheet = workbook.getSheetAt(2);
		int position = 0;
		
		Iterator it = contributions1.entrySet().iterator();
		while (it.hasNext()) {
			Iterator<Row> rowIterator = sheet.iterator();
	        Map.Entry pair = (Map.Entry)it.next();
	        while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				for (Cell cell : row) {
					switch (cell.getCellType()) {
					case Cell.CELL_TYPE_STRING:
						if(projectName1.equals(cell.getStringCellValue())){
							position = cell.getColumnIndex();
							break;
						}else
							break;
					case Cell.CELL_TYPE_NUMERIC:
						if(((Student) pair.getKey()).getGtid().equals(String.valueOf((int)cell.getNumericCellValue()))){
							row.getCell(position).setCellValue((int) pair.getValue());
						}
						break;
					}

				}
			}
	    }
		
		FileOutputStream fileOut = null;
		try {
			fileOut = new FileOutputStream(database1);
		} catch (FileNotFoundException e) {
			System.out.println("File was not found");
			e.printStackTrace();
		}
	    try {
			workbook.write(fileOut);
		} catch (IOException e) {
			System.out.println("File could not be written to");
			e.printStackTrace();
		}
	    
	    try {
			fileOut.close();
		} catch (IOException e) {
			System.out.println("File could not be closed");
			e.printStackTrace();
		}
		
		
	}
	
	

	public String getFormula() {		
		return formula;
	}

	public int calculateOverallGrade(int aT, int aA, int aP) throws GradeFormulaException{
		double overallGrade = 0;
		ScriptEngineManager manager = new ScriptEngineManager();
		ScriptEngine engine = manager.getEngineByName("js");
		String AT, AA, AP;
		AT = String.valueOf(aT);
		AA = String.valueOf(aA);
		AP = String.valueOf(aP);
		String gradeFormula = getFormula();
		
		if(!this.containsOperators(gradeFormula) || !this.containsVariables(gradeFormula))
			throw new GradeFormulaException("Formula is incorrect");		
		
		gradeFormula = gradeFormula.replaceAll("AT", AT).replaceAll("AA", AA).replaceAll("AP", AP);
		try {
			overallGrade = (double) engine.eval(gradeFormula);
		} catch (ScriptException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return (int) Math.round(overallGrade);
	}
	
	public boolean containsOperators(String string){
		String[] data = string.split(operators);
		if(data.length == 1)
			return true;
		if(!string.contains("+") && !string.contains("-") && !string.contains("*") && !string.contains("/"))
			return false;
		return true;
	}
	
	public boolean containsVariables(String string){
		String[] data = string.split(operators);
		if(data.length == 1)
			return true;
		for(int i=0;i<data.length;i+=2)
		{
			if(!data[i].equals("AA") && !data[i].equals("AP") && !data[i].equals("AT"))
		    		return false;
		}
		return true;
	}
	
	
	public boolean gtIdMatchesValue(String gtId, String value){
		if(gtId.equals(value))
				return true;
		else
			return false;
	}

	

	

}
