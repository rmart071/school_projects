package edu.gatech.seclass.gradescalculator;

public class Student {

	public String name;
	public String email;
	public String gtid;
	public int attendance;
	public String team;

	public Student(String name, String gtid) {
		this.name = name;
		this.gtid = gtid;
	}

	public String getName() {
		return name;
	}

	public String getGtid() {
		return gtid;
	}

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}

	public void setAttendance(int attendance) {
		this.attendance = attendance;
	}

	public int getAttendance() {
		return attendance;
	}
	
	public String getEmail(){
		return email;
	}
	
	public void setEmail(String email){
		this.email = email;
	}

}
