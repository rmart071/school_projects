package edu.gatech.seclass;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class MyStringTest {

	private MyStringInterface mystring;

	@Before
	public void setUp() throws Exception {
		mystring = new MyString();
	}

	@After
	public void tearDown() throws Exception {
		mystring = null;
	}

	@Test
	public void testGetConsonants1() {
		mystring.setString("I like vowels better than consonants");
		assertEquals("lkvwlsbttrthncnsnnts", mystring.getConsonants());
	}

	@Test
	public void testGetConsonants2() {
		mystring.setString("I love programming");
		assertEquals("lvprgrmmng", mystring.getConsonants());
	}

	@Test
	public void testGetConsonants3() {
		mystring.setString("Software Engineer");
		assertEquals("Sftwrngnr", mystring.getConsonants());
	}

	@Test
	public void testGetConsonants4() {
		mystring.setString("Georgia Tech");
		assertEquals("GrgTch", mystring.getConsonants());
	}

	@Test
	public void testNumberOfConsonants1() {
		mystring.setString("I like vowels better than consonants");
		assertEquals(20, mystring.numberOfConsonants());
	}

	@Test
	public void testNumberOfConsonants2() {
		mystring.setString("I love programming");
		assertEquals(10, mystring.numberOfConsonants());
	}

	@Test
	public void testNumberOfConsonants3() {
		mystring.setString("Software Engineer");
		assertEquals(9, mystring.numberOfConsonants());
	}

	@Test
	public void testNumberOfConsonants4() {
		mystring.setString("Georgia Tech");
		assertEquals(6, mystring.numberOfConsonants());
	}

	@Test
	public void testGetCharacter1() {
		mystring.setString("I like vowels better than consonants");
		assertEquals('e', mystring.getCharacter(16));
	}

	@Test
	public void testGetCharacter2() {
		mystring.setString("I love programming");
		assertEquals('r', mystring.getCharacter(9));
	}

	@Test(expected = Exception.class)  
	public void testGetCharacter3() {
		mystring.setString("Software Engineer");
		mystring.getCharacter(100);
	}

	@Test(expected = Exception.class)  
	public void testGetCharacter4() {
		mystring.setString("Georgia Tech");
		mystring.getCharacter(100);
	}

	@Test
	public void testFlipCaseInSubstring1() {
		mystring.setString("I Like Vowels Better Than Consonants");
		mystring.flipCaseInSubstring(7, 21);
		assertEquals("I Like vOWELS bETTER Than Consonants", mystring.getString());	
	}

	@Test
	public void testFlipCaseInSubstring2() {
		mystring.setString("Georgia Tech");
		mystring.flipCaseInSubstring(3, 8);
		assertEquals("GeORGIA Tech", mystring.getString());
	}

	@Test(expected = Exception.class)  
	public void testFlipCaseInSubstring3() {
		mystring.setString("Software Engineer");
		mystring.flipCaseInSubstring(0,5);
	}

	@Test(expected = Exception.class)  
	public void testFlipCaseInSubstring4() {
		mystring.setString("Georgia Tech");
		mystring.flipCaseInSubstring(3,0);
	}
}
