package edu.gatech.seclass;

import java.util.HashSet;
import java.util.Set;

public class MyString implements MyStringInterface{
	String mytest = new String();

	@Override
	public void setString(String string) {
		mytest = string;
	}

	@Override
	public String getString() {
		return mytest;
	}

	@Override
	public String getConsonants() {
		String group = "";
		char ch;
		Set<Character> vowels = new HashSet<Character>();
	    for (char ch2 : "aeiou".toCharArray()) {
	        vowels.add(ch2);
	    }
		for(int x = 0; x < mytest.length(); x++){
			ch = mytest.charAt(x);
			if(Character.isLetter(ch)){
				if(!vowels.contains(Character.toLowerCase(ch)))
					group +=ch;
			}
	
		}			
		return group;
	}

	@Override
	public int numberOfConsonants() {
		int counter = 0;
		char ch;
		Set<Character> vowels = new HashSet<Character>();
	    for (char ch2 : "aeiou".toCharArray()) {
	        vowels.add(ch2);
	    }
	   
		for(int x = 0; x < mytest.length(); x++){
			ch = mytest.charAt(x);
			if(Character.isLetter(ch)){
				if(!vowels.contains(Character.toLowerCase(ch)))
					counter++;
			}
		}
		return counter;
	}

	@Override
	public char getCharacter(int position) throws IllegalArgumentException, IllegalIndexException {
		if(position <= 0)
			throw new IllegalArgumentException();
		if(mytest.length() < position)
			throw new IllegalIndexException();
		
		char holder = mytest.charAt(position-1);
		System.out.println(holder);
		return holder;
	}

	@Override
	public void flipCaseInSubstring(int startPosition, int endPosition)
			throws IllegalArgumentException, IllegalIndexException {
		//use isUpperCase and isLowerCase to determine what to change for the character
		if(startPosition <= 0 || endPosition <= 0 || startPosition > endPosition)
			throw new IllegalArgumentException();
		
		if(mytest.length() < endPosition)
			throw new IllegalIndexException();
		
		String temp = "";
		String holder = mytest.substring(startPosition -1, endPosition);
		for(int x = 0; x < holder.length(); x++){
			char ch = holder.charAt(x);
			if(Character.isUpperCase(ch)){
				temp += Character.toLowerCase(ch);
			}else if(Character.isLowerCase(ch)){
				temp += Character.toUpperCase(ch);
			}else
				temp += ch;
		}
		mytest = mytest.substring(0,startPosition-1) + temp + mytest.substring(endPosition);

	}

	public static void main(String[] args) {
		MyString m = new MyString();
		m.setString("Ricardo Martinez");
		System.out.println(m.getConsonants());
		m.flipCaseInSubstring(3, 6);
		System.out.println(m.getString());

	}

}
