# School Assignments #

Contains mainly assignments, but also contains a GradesCalculator Program.

### Grades Calculator Program ###

* Reads data from excel spreadsheet
* Create functionality to handle new data (add, delete, edit from spreadsheet)
* Calculate average grades for student
* Interacted with interface to show results
* Create Test Cases to verify functionality correctness