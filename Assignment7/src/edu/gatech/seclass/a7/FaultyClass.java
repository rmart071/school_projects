package edu.gatech.seclass.a7;

public class FaultyClass {
	
	public static int faultyMethod1(int a , int b){
		int result = 0;
		if (b != 0)
			result = b - a;
		if(b > a)
			result = b/a;

		return result;
	}
	
	public static int faultyMethod2(int a, int b){
		int result = 0;
		if(a >= b)
			result = a/b;
		else
			result = a+b;
		return result;
	}
	
	public static int faultyMethod3(int a, int b){
		int result;
		if(a >= b)
			result = b/a;
		else if(a != 0)
			result = a + b;
		else
			result = 0;
		return result;
	}
	
	
	public static int faultyMethod4(int a, int b){
		int result = a+b;
		
		if(result == 0)
			result = a * b;
		else if(a > b)
			result = a/b;
		
		return result;
	}

}
