package edu.gatech.seclass.a7;

import static org.junit.Assert.*;

import org.junit.Test;

public class FaultyClassTestSC1{

	@Test
	public void testFaultyMethod1() {
		//should reveal 100% statement coverage and not reveal the fault
		//in this test case a = 3, b = 6
		assertEquals(2, FaultyClass.faultyMethod1(3, 6));
	}


}
