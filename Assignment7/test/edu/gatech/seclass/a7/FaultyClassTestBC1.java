package edu.gatech.seclass.a7;

import static org.junit.Assert.*;

import org.junit.Test;

public class FaultyClassTestBC1 {

	@Test
	public void testFaultyMethod1_1() {
		//test case 1 a = 0, b = 6
		assertEquals(0, FaultyClass.faultyMethod1(0, 6));
	}
	
	@Test
	public void testFaultyMethod1_2() {
		//test case 2: a = 1, b = 2
		assertEquals(2, FaultyClass.faultyMethod1(1, 2));
	}
	
	@Test
	public void testFaultyMethod1_3() {
		//test case 2: a = 1, b = 0
		assertEquals(0, FaultyClass.faultyMethod1(1, 0));
	}


}
