package edu.gatech.seclass.a7;

import static org.junit.Assert.*;

import org.junit.Test;

public class FaultyClassTestPC3 {

	//path 1
		@Test
		public void testFaultyMethod3_1() {
			// test case: a = -2, b = -2
			// will return -2/-2 = 1
			assertEquals(1,FaultyClass.faultyMethod3(-2, -2));
		}
		
		//path 2
		@Test
		public void testFaultyMethod3_2() {
			// test case: a = 2, b = 7
			// will return 2+7 = 9
			assertEquals(9,FaultyClass.faultyMethod3(2, 7));
		}
			
		//path 3
		@Test
		public void testFaultyMethod3_3() {
			// test case: a = -0, b = 1
			// will return 0
			assertEquals(0,FaultyClass.faultyMethod3(0, 1));
		}

}
