package edu.gatech.seclass.a7;

import static org.junit.Assert.*;

import org.junit.Test;

public class FaultyClassTestSC4 {

	@Test
	public void testFaultyMethod4_1() {
		// test case: a = 3, b = -3
		// will cover first statement
		assertEquals(-9, FaultyClass.faultyMethod4(3, -3));
	}
	
	@Test
	public void testFaultyMethod4_2() {
		// test case: a = 4, b = 0
		// will cover second statement but produce error
		assertEquals(0, FaultyClass.faultyMethod4(4, 0));
	}
	
	@Test
	public void testFaultyMethod4_3() {
		// test case: a = 4, b = 1
		// will cover second statement
		assertEquals(4, FaultyClass.faultyMethod4(4, 1));
	}

}
