package edu.gatech.seclass.a7;

import static org.junit.Assert.*;

import org.junit.Test;

public class FaultyClassTestBC3 {

	@Test
	public void testFaultyMethod3_1() {
		// test case: a = 0, b = -2
		// will show fault since 0 is denominator
		assertEquals(0,FaultyClass.faultyMethod3(0, -2));
	}
	
	@Test
	public void testFaultyMethod3_2() {
		// test case: a = 4, b = 4
		// will go through first branch
		// will return 4/4 = 1
		assertEquals(1, FaultyClass.faultyMethod3(4, 4));
	}
	
	@Test
	public void testFaultyMethod3_3() {
		// test case: a = 3, b = 6
		// will pass first branch and enter second
		// will return 3+6 = 9
		assertEquals(9, FaultyClass.faultyMethod3(3, 6));
	}

	
	@Test
	public void testFaultyMethod3_4() {
		// test case: a = 0, b = 3
		// will through last branch
		// will return 0
		assertEquals(0, FaultyClass.faultyMethod3(0, 3));
	}

}
