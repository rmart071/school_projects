package edu.gatech.seclass.a7;

import static org.junit.Assert.*;

import org.junit.Test;

public class FaultyClassTestBC2a {

	@Test
	public void testFaultyMethod2_1() {
		// test case a = 4, and b = 2
		//should return 4/2 = 2
		assertEquals(2, FaultyClass.faultyMethod2(4, 2));
	}
	
	@Test
	public void testFaultyMethod2_2() {
		// test case a = 1, b = 3
		// should return 1 + 3 = 4
		assertEquals(4, FaultyClass.faultyMethod2(1, 3));
	}

}
