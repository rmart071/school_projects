package edu.gatech.seclass.a7;

import static org.junit.Assert.*;

import org.junit.Test;

public class FaultyClassTestBC4 {

	@Test
	public void testFaultyMethod4_1() {
		// test case: a = 2, b = 2
		//will pass false on first branch and true on second branch
		assertEquals(-4, FaultyClass.faultyMethod4(2, -2));
	}
	
	@Test
	public void testFaultyMethod4_2() {
		// test case: a = -3, b = -5
		// will pass true on second branch
		assertEquals(0, FaultyClass.faultyMethod4(-3, -5));
	}
	
	@Test
	public void testFaultyMethod4_3() {
		// test case: a = -1, b = 0
		//will pass false on both the first and second branch
		assertEquals(-1, FaultyClass.faultyMethod4(-1, 0));
	}

}
