package edu.gatech.seclass.a7;

import static org.junit.Assert.*;

import org.junit.Test;

public class FaultyClassTestBC2b {

	@Test
	public void testFaultyMethod2_1() {
		// test case a = 0, b = -2
		// will cause an error because -2/0
		//0 is denominator
		assertEquals(0, FaultyClass.faultyMethod2(0, 0));
	}
	
	
	@Test
	public void testFaultyMethod2_2() {
		//test case a = -5, b = -10
		// result will return 2/4 =0.5 (0 since it is an integer)
		assertEquals(0, FaultyClass.faultyMethod2(-5, -10));
	}

}
